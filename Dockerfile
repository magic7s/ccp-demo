FROM node:8

# Create app directory
WORKDIR /usr/src/app

RUN cd /tmp/; \
    git clone --depth 1 https://github.com/aws-samples/lambda-refarch-imagerecognition.git; \
    cp -r lambda-refarch-imagerecognition/webapp/* /usr/src/app/; cd /usr/src/app

# Copy local config file edited with deployed stack outputs
COPY config.ts ./app/

RUN npm install

EXPOSE 3000
CMD [ "npm", "start" ]

