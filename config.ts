export interface Config {
  Region: string;
  S3PhotoRepoBucket: string;
  DDBImageMetadataTable: string;
  DDBAlbumMetadataTable: string;
  DescribeExecutionLambda: string;
  CognitoIdentityPool: string;
}

export const CONFIG: Config = {
  DDBAlbumMetadataTable : "photo-sharing-backend-AlbumMetadataDDBTable-1FSDYSWBUQJY7",
  CognitoIdentityPool : "us-west-2:162dc08b-0aa8-4370-95ba-ce7604da145d",
  Region : "us-west-2",   // might be replaced if you launched the template in a different region
  DDBImageMetadataTable : "photo-sharing-backend-ImageMetadataDDBTable-1WGTE9YTL5PDU",
  S3PhotoRepoBucket : "photo-sharing-backend-photorepos3bucket-cjukwxawhzo9",
  DescribeExecutionLambda : "photo-sharing-backend-DescribeExecutionFunction-ZUYITWLW5S40"
};
